package Exception_part5;

public class Main {
	public static void main(String[] args) {
		Refrigurator ref = new Refrigurator();
		try{
			ref.put("Apple");
			ref.put("Coke");
			ref.put("Egg");
			ref.put("Orange juice");
			
			System.out.println(ref.toString());
		}
		catch(FullException e){
			System.err.println("Refrigurator Full");
			
		}
	}
}
