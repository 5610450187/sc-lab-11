package Exception_part5;

import java.util.ArrayList;

public class Refrigurator {
	private int size = 3  ;
	ArrayList<String> things;
	public Refrigurator() {
		things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException{
		things.add(stuff);
		if (size < things.size()){
			throw new FullException();
		}

	}
	
	public String takeOut(String stuff){
		int index=0;
		String item = null;
		for (String string : things) {
			if(string.equals(stuff)){
				item = things.get(index);
				things.remove(index);
			}else {
				index++;
			}
		}
		return item;
	}
	
	public String toString(){
		String out="";
		for (String string : things) {
			out += string+"\n";
		}
		return out;
		
	}

}
