package HashMap;
import java.util.HashMap;
import java.util.StringTokenizer;


public class Wordcounter {
	private String message;
	HashMap<String,Integer> wordCount;
	StringTokenizer st;

	public Wordcounter(String m) {
		wordCount = new HashMap<String,Integer>();

		this.message = m;
	}
	public void count(){
		int count = 0;
		st = new StringTokenizer(message);
		while (st.hasMoreElements()){
			String str = (String)st.nextElement();
			if(wordCount.containsKey(str)){
				count = wordCount.get(str);
				count+=1;
				wordCount.put(str,count);
			}
			else {
				wordCount.put(str, 1);
			}

		}
	}
	public int hasWord(String word){
		if(!wordCount.containsKey(word)){
		return 0;
		}
		else{
			return wordCount.get(word);
		}
	}

}
